package user

import (
	"database/sql"

	database "udemy-golang/db"
)

type User struct {
    ID       int
    Username string
    Password string
}

func FindByUsername(username string) (*User, error) {
    row := database.DB.QueryRow("SELECT id, username, password FROM users WHERE username = $1", username)

    var user User
    err := row.Scan(&user.ID, &user.Username, &user.Password)
    if err != nil {
        if err == sql.ErrNoRows {
            return nil, nil
        }
        return nil, err
    }

    return &user, nil
}
