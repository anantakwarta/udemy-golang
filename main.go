package main

import (
	"database/sql"
	"fmt"
	"log"
	database "udemy-golang/db"
	"udemy-golang/middleware"

	"github.com/gofiber/fiber/v2"
	_ "github.com/lib/pq"
)

// Course struct
type Course struct {
	ID     string `json:"id"`
	Title   string `json:"title"`
	Subtitle   string `json:"subtitle"`
	Creator string `json:"creator"`
	DateCreated    string `json:"date_created"`
	Price    int `json:"price"`
	Learns    string `json:"learns"`
}

// Review struct
type Review struct {
	ID     int `json:"id"`
	UserID   sql.NullInt64 `json:"user_id"`
	Name   string `json:"name"`
	CourseID int `json:"course_id"`
	Rating    int `json:"rating"`
	Helpful    int `json:"helpful"`
	Unhelpful    int `json:"unhelpful"`
	Reported    bool `json:"reported"`
	Review    string `json:"review"`
	CreatedAt    string `json:"createdat"`
}

// Reviews struct
type Reviews struct {
	Reviews []Review `json:"reviews"`
}

func main() {
	// Connect with database
	db := database.Connect()

	// Create a Fiber app
	app := fiber.New()
	app.Use(middleware.BasicAuth())
	app.Get("/course/:id", func(c *fiber.Ctx) error {
		// Select all Employee(s) from database
		id := c.Params("id")
		rows := db.QueryRow("SELECT * FROM courses WHERE id=$1", id)
		
		course := Course{}
		err := rows.Scan(
			&course.ID, 
			&course.Title, 
			&course.Subtitle, 
			&course.Creator, 
			&course.DateCreated, 
			&course.Price, 
			&course.Learns,
		);
		if err != nil {
			return c.Status(500).SendString(err.Error())
		}

		fmt.Print(course)

		return c.JSON(course)
	})

	app.Get("/course/:id/reviews", func(c *fiber.Ctx) error {
		// Select all Review(s) from database
		id := c.Params("id")
		rows, err := db.Query("SELECT * FROM reviews WHERE course_id=$1 limit 10", id)
		if err != nil {
			return c.Status(500).SendString(err.Error())
		}
		defer rows.Close()
		result := Reviews{}

		for rows.Next() {
			review := Review{}

			if err := rows.Scan(
				&review.ID, 
				&review.UserID, 
				&review.Name, 
				&review.CourseID, 
				&review.Rating, 
				&review.Helpful, 
				&review.Unhelpful, 
				&review.Reported, 
				&review.Review, 
				&review.CreatedAt, 
			); err != nil {
				return err // Exit if we get an error
			}

			// Append Review to Reviews
			result.Reviews = append(result.Reviews, review)
		}

		
		if err != nil {
			return c.Status(500).SendString(err.Error())
		}

		// fmt.Print(result)

		return c.JSON(result)
	})

	log.Fatal(app.Listen(":3000"))
}